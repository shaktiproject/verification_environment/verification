# RISC-V Compliance Test I-SH-01
#
# Copyright (c) 2017, Codasip Ltd.
# Copyright (c) 2018, Imperas Software Ltd. Additions
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#      * Redistributions of source code must retain the above copyright
#        notice, this list of conditions and the following disclaimer.
#      * Redistributions in binary form must reproduce the above copyright
#        notice, this list of conditions and the following disclaimer in the
#        documentation and/or other materials provided with the distribution.
#      * Neither the name of the Codasip Ltd., Imperas Software Ltd. nor the
#        names of its contributors may be used to endorse or promote products
#        derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Codasip Ltd., Imperas Software Ltd.
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Specification: RV32I Base Integer Instruction Set, Version 2.0
# Description: Testing instruction SH.

#include "compliance_test.h"
#include "compliance_model.h"


RVTEST_ISA("RV32I")

# Test Virtual Machine (TVM) used by program.


# Test code region
RVTEST_CODE_BEGIN

    RVMODEL_IO_INIT
    RVMODEL_IO_ASSERT_GPR_EQ(x31, x0, 0x00000000)
    RVMODEL_IO_WRITE_STR(x31, "# Test Begin\n")

  #ifdef TEST_CASE_1
    RVTEST_CASE(1,"// check ISA:=regex(.*I.*); def TEST_CASE_1=True")
    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part A1 - test base address + 0\n");

    # Address for test results
    la      x1, test_temp_data
    RVTEST_SIGBASE(x9, test_A1_res)
    # Set memory
    li      x31, 0xAAAABBBB
    sw      x31, 0(x1)

    # Test
    li      x2, 0x11F1F222
    sh      x2, 0(x1)
    
    lw      x3, 0(x1)
    //
    // Assert
    //
    RVMODEL_IO_CHECK()
    RVTEST_SIGUPD(x9, x3, 0xAAAAF222)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A1  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part A2 - test base address - 1\n");

    # Address for test results
    la      x5, test_temp_data + 1
    RVTEST_SIGBASE(x9, test_A2_res)
    # Clear memory
    sw      x0, -1(x5)

    # Test
    li      x25, 0xF33344F4
    sh      x25, 0xFFFFFFFF(x5)
    lw      x26, 0xFFFFFFFF(x5)

    RVTEST_SIGUPD(x9, x26, 0x000044F4)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A2  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part A3 - test base address + 1\n");

    # Address for test results
    la      x8, test_temp_data - 1
    RVTEST_SIGBASE(x9, test_A3_res)
    # Clear memory
    sw      x0, 1(x8)

    # Test
    li      x31, 0x55F5F666
    sh      x31, +1(x8)
    lw      x10, +1(x8)

    RVTEST_SIGUPD(x9, x10, 0x0000F666)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A3  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part A4 - test base address - 2048\n");

    # Address for test results
    la      x11, test_temp_data + 2048
    RVTEST_SIGBASE(x9, test_A4_res)
    # Clear memory
    sw      x0, -2048(x11)

    # Test
    li      x12, 0xF77788F8
    sh      x12, 0xFFFFF800(x11)
    lw      x13, 0xFFFFF800(x11)

    RVTEST_SIGUPD(x9, x13, 0x000088F8)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A4  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part A5 - test base address + 2047\n");

    # Address for test results
    la      x14, test_temp_data - 2047
    RVTEST_SIGBASE(x9, test_A5_res)
    # Clear memory
    sw      x0, 2047(x14)

    # Test
    li      x15, 0x99090AAA
    sh      x15, 0x7FF(x14)
    lw      x16, 0x7FF(x14)

    RVTEST_SIGUPD(x9, x16, 0x00000AAA)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A5  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part B - test base address + -4, -2, ... , 6\n");

    # Address for test results
    la      x17, test_temp_data
    RVTEST_SIGBASE(x9, test_B_res)
    # Test
    li      x18, 0x1111CC0C
    li      x19, 0x22220BBB
    li      x20, 0x33330EEE
    li      x21, 0x4444DD0D
    li      x22, 0x777700F0
    li      x23, 0x66660FFF

    # Store results
    sh      x18, -4(x17)
    sh      x19, -2(x17)
    sh      x20, 0(x17)
    sh      x21, 2(x17)
    sh      x22, 4(x17)
    sh      x23, 6(x17)

    lw      x16, -4(x17)
    lw      x15, 0(x17)
    lw      x14, 4(x17)

    RVTEST_SIGUPD(x9, x16, 0x0BBBCC0C)
    RVTEST_SIGUPD(x9, x15, 0xDD0D0EEE)
    RVTEST_SIGUPD(x9, x14, 0x0FFF00F0)

    RVMODEL_IO_WRITE_STR(x31, "# Test part B  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part C - test store x0\n");

    # Address for test results
    la      x22, test_temp_data
    RVTEST_SIGBASE(x9, test_C_res)
    # Set memory
    li      x1, 0x87654321
    sw      x1, 0(x22)

    # Test
    li      x0, 0x12345678
    sh      x0, 0(x22)
    lw      x2, 0(x22)

    RVTEST_SIGUPD(x9, x2, 0x87650000)

    RVMODEL_IO_WRITE_STR(x31, "# Test part C  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part D1 - test for forwarding (to address register)\n");

    # Address for test data
    la      x21, test_D1_data
    RVTEST_SIGBASE(x9, test_D1_res)
    # Clear memory
    lw      x1, 0(x21)
    sw      x0, 0(x1)

    # Test
    li      x19, 0x11223344
    lw      x23, 0(x21)
    sh      x19, 0(x23)

    lw      x20, 0(x23)

    RVTEST_SIGUPD(x9, x20, 0x00003344)

    RVMODEL_IO_WRITE_STR(x31, "# Test part D  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part D2 - test for forwarding (to data register)\n");

    # Address for test data
    la      x23, test_D2_data
    la      x24, test_temp_data
    RVTEST_SIGBASE(x9, test_D2_res)
    # Clear memory
    sw      x0, 0(x24)

    # Test
    lw      x25, 0(x23)
    sh      x25, 0(x24)
    lw      x26, 0(x24)

    RVTEST_SIGUPD(x9, x26, 0x0000DEF0)

    RVMODEL_IO_WRITE_STR(x31, "# Test part E  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part E1 - test war hazard (data register)\n");

    # Address for test results
    la      x26, test_temp_data
    RVTEST_SIGBASE(x9, test_E1_res)
    # Clear memory
    sw      x0, 0(x26)

    # Test
    li      x25, 0x76543210
    sh      x25, 0(x26)
    mv      x25, x0
    lw      x24, 0(x26)

    RVTEST_SIGUPD(x9, x24, 0x00003210)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A1  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part E2 - test war hazard (address register)\n");

    # Address for test results
    la      x28, test_temp_data
    RVTEST_SIGBASE(x9, test_E2_res)
    # Clear memory
    sw      x0, 0(x28)

    # Test
    li      x27, 0x89ABCDEF
    sh      x27, 0(x28)
    addi    x28, x28, -4
    lw      x29, 4(x28)

    RVTEST_SIGUPD(x9, x29, 0x0000CDEF)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A2  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part F - test raw hazard in memory\n");

    # Address for test results
    la      x29, test_temp_data
    RVTEST_SIGBASE(x9, test_F_res)
    # Clear memory
    sw      x0, 0(x29)
    sw      x0, 4(x29)

    # Test
    li      x27, 0x14725836
    sh      x27, 0(x29)
    lw      x30, 0(x29)
    sh      x30, 4(x29)

    lw      x26, 0(x29)
    lw      x25, 4(x29)

    RVTEST_SIGUPD(x9, x26, 0x00005836)
    RVTEST_SIGUPD(x9, x25, 0x00005836)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A3  - Complete\n");

    # ---------------------------------------------------------------------------------------------
    RVMODEL_IO_WRITE_STR(x31, "# Test part G - test waw hazard in memory\n");

    # Address for test results
    la      x1, test_temp_data
    RVTEST_SIGBASE(x9, test_G_res)
    # Clear memory
    sw      x0, 0(x1)

    # Test
    li      x2, 0x96385201
    li      x3, 0x25814963
    sh      x2, 0(x1)
    sh      x3, 0(x1)
    lw      x4, 0(x1)

    RVTEST_SIGUPD(x9, x4, 0x00004963)

    RVMODEL_IO_WRITE_STR(x31, "# Test part A4  - Complete\n");

    RVMODEL_IO_WRITE_STR(x31, "# Test End\n")
  #endif
 # ---------------------------------------------------------------------------------------------
    # HALT
    RVMODEL_HALT

RVTEST_CODE_END

# Input data section.
    .data
    .align 4

test_D1_data:
    .word test_temp_data
test_D2_data:
    .word 0x9ABCDEF0
    .fill 1, 4, -1
test_temp_data:
    .fill 2, 4, -1

# Output data section.
RVMODEL_DATA_BEGIN
    .align 4

test_A1_res:
    .fill 1, 4, -1
test_A2_res:
    .fill 1, 4, -1
test_A3_res:
    .fill 1, 4, -1
test_A4_res:
    .fill 1, 4, -1
test_A5_res:
    .fill 1, 4, -1
test_B_res:
    .fill 3, 4, -1
test_C_res:
    .fill 1, 4, -1
test_D1_res:
    .fill 1, 4, -1
test_D2_res:
    .fill 1, 4, -1
test_E1_res:
    .fill 1, 4, -1
test_E2_res:
    .fill 1, 4, -1
test_F_res:
    .fill 2, 4, -1
test_G_res:
    .fill 1, 4, -1

RVMODEL_DATA_END

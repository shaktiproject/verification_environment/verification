#include "compliance_test.h"
#include "compliance_model.h"

RVTEST_ISA("RV64M")
#RMODEL_RV32M
# Test Virtual Machine (TVM) used by program.


# Test code region.
RVTEST_CODE_BEGIN

    RVMODEL_IO_INIT
    RVMODEL_IO_ASSERT_GPR_EQ(x31, x0, 0x00000000)
    RVMODEL_IO_WRITE_STR(x31, "# Test Begin\n")

    # ---------------------------------------------------------------------------------------------
    RVTEST_CASE(1,"check ISA:=regex(.*I.*); \
                        def TEST_CASE_1=True")
    RVMODEL_IO_WRITE_STR(x31, "#Test-1 \n");
    RVTEST_SIGBASE(x2, test_A1_res)

    la t1, mtvec_handler
    csrw mtvec, t1
  
.option norvc
noexcep:

    // This is the address of the boot-section which is accessible for loads/stores
    li t2, 0x1000
    # since no protection is added. The following loads should not generate exception.
    lwu t3, 0(t2)
    lwu t3, 4(t2)
    lwu t3, 8(t2)

    # write pmpaddress with boot-code section.
    li t1, 0x1010
    csrw 0x3b0, t1
    csrr t4, 0x3b0
    RVTEST_SIGUPD(x2, t4, 0x20)
   
 # protect boot-code section and below it against any writes/reads
    # remember in machine mode the protection is only applied when the cfg entry is locked.
    # Otherwise protection is applied only to the supervisor and user mode access.

    li t1, 0x83 
    csrw 0x3a0, t1
    csrr t4,0x3a0
    RVTEST_SIGUPD(x2, t4, 0x03)

 # write pmpaddress with boot-code section.
    li t1, 0x1011
    csrw 0x3b1, t1
    csrr t4, 0x3b1
    RVTEST_SIGUPD(x2, t4, 0x30)
   
 # protect boot-code section and below it against any writes/reads
    # remember in machine mode the protection is only applied when the cfg entry is locked.
    # Otherwise protection is applied only to the supervisor and user mode access.

    li t1, 0x8b
    csrw 0x3a0, t1
    csrr t4,0x3a0
    RVTEST_SIGUPD(x2, t4, 0x0b)

 # the following loads should cause a trap and go to mtvec handler
    lwu t3, 0(t2)
    lwu t3, 4(t2)
    lwu t3, 8(t2)
    sw t3, 0(t2)
    sw t3, 4(t2)
    sw t3, 8(t2)

fail:
    RVMODEL_IO_WRITE_STR(x31,"# Test Failed\n")
    j HALT
    
pass:
    RVMODEL_IO_WRITE_STR(x31,"# Test Passed\n")
    j HALT

HALT:
    RVMODEL_HALT

RVTEST_CODE_END

# ---------------------------------------------------------------------------------------------
.align 4
mtvec_handler:
  
    # increment return address
    csrr    t1 , mepc
    addi    t1 , t1 , 4
    csrw    mepc , t1
    addi t0, t0, -1
    beqz t0, pass
    mret



RVMODEL_DATA_BEGIN

test_A1_res:
    .fill 8, 4, -1



RVMODEL_DATA_END

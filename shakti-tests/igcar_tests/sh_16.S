######################TEST#############################################
#1.This test check halfword R/W operations on 16 bit slave memory
#####################################################################################
# include"riscv_test.h"
#include "test_macros.h"
RVTEST_RV64U
RVTEST_CODE_BEGIN


  li sp,0x00003FF8

  li x11,0x000000   
  li x12,0x000010   
	
flush:
	li x5, 0x0
	sw x5,0(x11)
	addi x11,x11,0x4
	bge x12,x11,flush

	li x11,0x30000000   
	li x12,0x30000010 

  mv x13,x11
#Test checks half word R/W operations to 16 bit big-endian slave
fillaa:
    
  li x5, 0x0012
  sh x5,0(x13)
  addi x13,x13,0x2
  bge x13,x12,fillaa   
  mv x13,x11

testaa:	
            
  li TESTNUM,0x02
  lhu x14,0(x13)
  li x15,0x0012
  bne x14,x15,fail
  addi x13,x13,0x02
  bge x13,x12,testaa   


TEST_PASSFAIL

RVTEST_CODE_END

  .data
RVTEST_DATA_BEGIN

TEST_DATA

RVTEST_DATA_END




#*****************************************************************************
# Purpose:  Checks if exception with mcause=2 occurs when illegal instruction
#           is seen
#-----------------------------------------------------------------------------

#include "riscv_test.h"
#include "test_macros.h"

RVTEST_RV64M
RVTEST_CODE_BEGIN

  .option norvc

  li TESTNUM, 2
bad2:
  .word 0x00000000

ret:

  TEST_PASSFAIL

.align 8
.global mtvec_handler
mtvec_handler:
  li t1, CAUSE_ILLEGAL_INSTRUCTION
  csrr t0, mcause
  bne t0, t1, fail
  csrr t0, mepc
  la t1, bad2
  beq t0, t1, 2f
  j fail
2:
  la t0, ret
  csrw mepc, t0
  mret

RVTEST_CODE_END

  .data
RVTEST_DATA_BEGIN

  TEST_DATA

RVTEST_DATA_END

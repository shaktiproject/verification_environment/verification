// "The MPV bit (Machine Previous Virtualization Mode) is written by the implementation whenever
// a trap is taken into M-mode. Just as the MPP bit is set to the privilege mode at the time of the
// trap, the MPV bit is set to the value of the virtualization mode V at the time of the trap. When an
// MRET instruction is executed, the virtualization mode V is set to MPV, unless MPP=3, in which
// case V remains 0.                        (mstatus)"


#include "riscv_test.h"
#include "test_macros.h"

RVTEST_RV64M
RVTEST_CODE_BEGIN

test_Start:
  #-------------------------------------------------------------
  # Arithmetic tests
  #-------------------------------------------------------------

//    # Addresses for test data and results
//    la      x1, test_A1_data
//    la      x2, test_A1_res


    csrr    t4,misa
    li      t3 , 1 << 7
    csrs    misa,t3              //Set bit 7 of misa as 1 to enter hypervisor 
    csrr    t3,misa
    li TESTNUM, 1
     
    csrr  x7, mstatus
    andi  x7, x7, 0x1000        // Sice we havent moved to any mode, P=0 by default)


//     # Store results
//     sw      x7, 0(x2)


//     RVTEST_IO_ASSERT_GPR_EQ(x2, x7, 0x00)


//     RVTEST_IO_WRITE_STR(x31, "# Test Pass\n");
     



  TEST_PASSFAIL

RVTEST_CODE_END

  .data
RVTEST_DATA_BEGIN

  TEST_DATA

RVTEST_DATA_END


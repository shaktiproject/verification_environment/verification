# SHAKTI Verification 
This repository maintains the verification framework for all [SHAKTI](http://shakti.org.in/) cores.

## Contents
* [Setup](#setup)
  - [Dependencies](#deps)
* [Directory Structure](#dir-struct)
* [Verification Framework usage](#verif-usage)
* [Cheat-sheet for ''test'' target](#test-target)
* [Cheat-sheet for ''regress'' target](#regress-target)
* [Cheat-sheet for ''aapg'' target](#aapg-target)
* [Cheat-sheet for ''torture'' target](#torture-target)

## Setup <a name="setup"></a>
The design is assumed to be compiled before using the verification setup. As an example, SHAKTI **c-class** core compilation steps can be found [here](https://gitlab.com/shaktiproject/cores/c-class).
#### Dependencies <a name="deps"></a>
The following versions were used during the development and simulation
* Scripts: python 3.5.2, perl 5
* Dependencies for [riscv-tests](https://github.com/riscv/riscv-tools)
* Dependencies for [riscv-torture](https://github.com/ucb-bar/riscv-torture)

## Directory Structure <a name="dir-struct"></a>
```
verification
    ├── patches                 # Fixes env variables for verificaton
    ├── docs                    # docs
    ├── dts                     # Generates boot code
    ├── riscv-tests             # RISC-V ISA and benchmark tests
    ├── riscv-torture           # Random Torture tests
    ├── shakti-tests            # SHAKTI directed tests
    ├── verif-scripts           # Verification framework used for SHAKTI cores
```
   
Apart from these we use the following infrastructure for verification
* [mod-spike](https://gitlab.com/shaktiproject/tools/mod-spike) is the reference model that is used for RISC-V ISA level checking
* [aapg](https://gitlab.com/shaktiproject/tools/aapg) is the in-house RISC-V random test generator for comprehensive instruction, exception generation and privileged mode switching
* [rita](https://gitlab.com/shaktiproject/tools/rita) RISC-V trace analyser to quantify simulation at the ISA level *(integration work-in-progress)*
* [riscof](https://gitlab.com/incoresemi/riscof) RISC-V Compliance Framework, a test and RISC-V compliance checking *(work-in-progress)*

## Verification Framework usage <a name="verif-usage"></a>

All SHAKTI cores use this repository as a submodule for its verification. Make targets are supported accordingly. 
Example c-class usage:
``` 
git clone --recursive https://gitlab.com/shaktiproject/cores/c-class.git 
cd c-class
make patch    
```
The following sections will give a cheat-sheet for usage. 

## Cheat-sheet for 'test' target <a name="test-target"></a>

``test`` target compiles, simulates any test and checks with the reference model at the ISA level
```
$ make test opts="--help"
```
| command          | description |
| -----------------| ---------------- |
| make test opts="--help"| Displays usage options |
| make test opts="--test=add --suite=rv64ui" | run a test |

## Cheat-sheet for 'regress' target <a name="regress-target"></a>

``regress`` provides regression run of the tests
```
$ make regress opts="--help"
```
| command          | description |
| -----------------| ---------------- |
| make regress                                          |  lists the ``riscv-tests`` |
| make regress opts="--clean"                           | cleans all regression related files | 
| make regress opts="--help"                            | Displays usage options |
| make regress opts="--filter=rv64mi"                   | regx filter| 
| make regress opts="--filter=rv64mi --res=pass"        | filters the rv64mi tests that passed| 
| make regress opts="--filter=rv64mi --sub"             | runs filtered tests| 
| make regress opts="--filter=mulh --parallel=10 --sub"    | runs in parallel | 

## Cheat-sheet for 'aapg' target <a name="aapg-target"></a>
``aapg`` generates random [aapg](https://gitlab.com/shaktiproject/tools/aapg) tests
```
$ make aapg opts="--help"
```
| command          | description |
| -----------------| ---------------- |
| make aapg opts="--help"| Displays usage options |
| make aapg opts="--clean"                           | cleans all AAPG generation files | 
| make aapg opts="--list_configs" | lists configs |
| make aapg opts="--config=dataProcessing --test_count=5" | generates tests |
| make aapg opts="--sub" | generates and simulates a single AAPG test |

## Cheat-sheet for 'torture' target <a name="torture-target"></a>
``torture`` generates random [riscv-torture](https://github.com/ucb-bar/riscv-torture) tests
```
$ make torture opts="--help"
```

| command          | description |
| -----------------| ---------------- |
| make torture opts="--help"| Displays usage options |
| make torture opts="--clean"                           | cleans all torture generation files | 
| make torture opts="--list_configs" | lists configs |
| make torture opts="--test_count=20 --parallel" | generates 20 riscv-torture tests with bringup.config in parallel|
| make torture opts="--sub"| generates and simulates a single riscv-torture test|


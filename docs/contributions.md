# Verification Contributions
SHAKTI Verification has the following projects open for contributions. For contributions and more details, please mail: shakti.iitm@gmail.com or lavanya.jagan@gmail.com

## Peripheral Verification
- Development of verification environment using SV, UVM for SHAKTI peripherals 
- Peripheral design is in BSV/Verilog/SV
- Verification IPs need to be developed for the following 
    * USB 2.0 (Design at https://opencores.org/projects/usb)
    * Ethernet (Design at https://opencores.org/projects/ethmac10g)
    * UART 
    * I2C
    * QSPI
    * AXI4
    * ONFI
    * SDRAM
    * DMA
- SHAKTI IPs are at https://gitlab.com/shaktiproject/uncore/devices

## RISC-V Privileged Specification Tests
- Development of RISC-V assembly tests following Privileged Specification
- RISC-V Compliance Test format to be followed
- Test environment already in place

## RISC-V Compliance Suite Enhancements
- Development of RISC-V assembly tests following RISC-V User Specification
- Various RISC-V configurations to be taken into account
- Test environment already in place

## SHAKTI Micro-architecture tests
Development of C/assembly tests targeting
- Branch prediction
- Cache thrashing
- Test execution bypassing cache
- Various nested exception sequences
- Performance counters integration
- Interrupts

## Bluecheck based module verification

## RiTA
- Python based tool
- https://gitlab.com/shaktiproject/tools/rita

## Coq Proof Development

## RISC-V Formal Models
- Forvis model

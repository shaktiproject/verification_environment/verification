# Verification Plan 
(Live document: More Details to be added/reviewed)
## RISC-V Core verification
- Based on riscv-user-spec, riscv-priv-spec
    - shakti core verification environment running riscv-tests, riscv-torture, aapg, compliance tests with register updates checked after every instruction
    - Additional Hypervisor mode
        * Directed self-checking tests to be developed
    - Micro-architectural level
        * Pipeline stage level
        * Memory subsystem: d-cache, i-cache, tlb
## Cache coherence protocol verification

## Peripherals 
- Block level verification

## SoC Integration Verification
- To be decided
## Verification targetting safety critical features
- Assertions at BSV level [Rahul]


## Milestones


|Milestone | Verification | Duration |
|-- | --------  | -------- |
|Phase 1 | RISC-V Core    |  2 months  |
|Phase 2 | Cache Coherence |   3 months   |
|Phase 3 | Peripherals    |  4 months  |
|Phase 4 | SoC   |    2 months |
|Phase 5 | Safety Critical Features    |   |

## Resources
* Lavanya J
* Anmol Sahoo

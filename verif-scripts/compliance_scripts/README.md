
# Generate Signatures

## Run
To run the signature generator, execute the following command:
```bash
$ bash run_generate_signatures.bash [-t path-to-dir-with-test-codes] [-c path-to-conf-file] [-o dir-for-generated-signatures]
```
From the folder containing the run_generate_signature.bash script.

## Inputs

| command          | description |
| -----------------| ---------------- |
| -c \| --conf                              | location of the configuration file on the basis of which the signature would be generated. |
| -t \| --test                | location of the directory with all the test codes for which the corresponding signature has to be generated. |
| -o \| --out                  | location of the directory in which all the generated signatures would be kept. The generated signatures would be of the form "<test_code_name>.reference_output". |

## Python Script 

The generate_signature.py is called in succession by the bash script to generate the signature for one test code.

#### Arguments

1. Test Code (\*.S)
2. Config File (\*.yaml)
3. Output Signature File (<test_code>.reference_output)

#### Syntax Checks

1. RVTEST_PART_START, RVTEST_PART_SKIP, RVTEST_PART_END should have the same test case number.
2. Every RVTEST_PART_START should have a RVTEST_PART_END
3. The code line with skip should be of the form: RVTEST_PART_SKIP(\<test-number>, \<conf-key>:\<conf-value>)

#### Outputs

The output is a signature file with the signature being \<test-number> always except when the skip statement is satisfied
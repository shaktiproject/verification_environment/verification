#!/usr/bin/perl

#-----------------------------------------------------------
# makeCSmith.pl
# Generates random tests using csmith
#-----------------------------------------------------------

use strict;
use Getopt::Long;
use testRunConfig;
use scriptUtils;

checkSetup();
setEnvConfig();

my $simulator = getConfig("CONFIG_SIM");
my $testPath =  "$workdir/csmith";



# Parse options
GetOptions(
          qw(test_count=s)    => \my $test_count,
          qw(submit)          => \my $submit,
          qw(debug)          => \my $debug,
          qw(parallel=s)        => \my $parallel,
          qw(timeout=s) => \my $timeout,
          qw(help)            => \my $help,
          qw(clean)           => \my $clean
);

testRunConfig::setEnvConfig();

my $testCount;
my $testParallel;
my $timeout_value;


my $temp_path = `which csmith`;
my $csmith_path = `dirname $temp_path`;
chomp($csmith_path);
$csmith_path = "$csmith_path/..";

if ($parallel) {
  $testParallel = $parallel;
}
else {
  $testParallel = 0;
}

if ($debug) {
  setConfigValue("CONFIG_LOG", 1);
  $ENV{'CONFIG_LOG'} = 1;
}
else {
  if (defined $ENV{'CONFIG_LOG'}) {
    setConfigValue("CONFIG_LOG", 1);
    setConfigValue("CONFIG_LOG", $ENV{'CONFIG_LOG'});
  }
  else {
    setConfigValue("CONFIG_LOG", 0);
  }
}

if (!$timeout) {
  $timeout_value = "25s";
}
else {
  $timeout_value = $timeout;
}

# Test count
if ($test_count) {
  $testCount = $test_count;
}
else {
  $testCount = 1;
}

if ($submit) {
  checkBins();
}

# Prints command line usage of script
if ($help) {
  printHelp();
  exit(0);
}

# Clean script generated outputs
if ($clean) {
  doClean();
  exit(0);
}

my $float = "";
if ($ENV{'CONFIG_ISA'} =~ /F/)  {
  $float = "--float";
}

my $m_opt= "-m64";
if ($ENV{'CONFIG_ISA'} =~ /RV32/) {
  $m_opt = "-m32";
}
systemCmd("mkdir -p $testPath");
systemCmd("rm -rf $testPath/*");
chdir("$testPath");
doDebugPrint("cd $testPath\n");
my $count = $testCount;
while ($count) {
  if (($testParallel > 0) && ($testParallel < $count)) {
    foreach my $i (1 .. $testParallel) {
      my @date = `date +%d%m%Y%s`; chomp(@date);
      my $testName = join("", "csmith_", $date[0], "_test$i");
  
      my $pid;
      $pid = fork();
      die if not defined $pid;
      if (not $pid) {
        my $ret = systemCmd("csmith --no-packed-struct $float -o $testPath/$testName.c");
        setpgrp(0,0);
        chdir('/');
        exit($ret);
      }
    }
    1 while (wait() != -1);
    $count = $count - $testParallel;
  }
  elsif($testParallel >= $count) {
    foreach my $i (1 .. $testCount) {
      my @date = `date +%d%m%Y%s`; chomp(@date);
      my $testName = join("", "csmith_", $date[0], "_test$i");
  
      my $pid;
      $pid = fork();
      die if not defined $pid;
      if (not $pid) {
        my $ret = systemCmd("csmith --no-packed-struct $float -o $testPath/$testName.c");
        systemCmd("gcc $m_opt -w -Os -I /tools/csmith/runtime -I $csmith_path/include/csmith-2.3.0
                       $testPath/$testName.c -o $testPath/$testName.elf");
        `timeout --foreground $timeout_value $testPath/$testName.elf`;
        my $ret1 = $?;
        if ($ret1 != 0) {
          systemCmd("rm -rf $testPath/$testName.c");
        }
        setpgrp(0,0);
        chdir('/');
        exit($ret);
      }
    }
    1 while (wait() != -1);
    $count = 0;
  }
  else {
    my @date = `date +%d%m%Y%s`; chomp(@date);
    my $testName = join("", "csmith_", $date[0], "_test$count");
    systemCmd("csmith --no-packed-struct $float -o $testPath/$testName.c");
    $count--;
  }
}

createTestList("$workdir/csmith.list", "$testPath", "csmith", "p", "c");
if ($submit) {
  if ($debug) {
    systemCmd("perl -I $scriptPath $scriptPath/makeRegress.pl --list=csmith.list --sub --debug --parallel=$testParallel");
  }
  else {
    system("perl -I $scriptPath $scriptPath/makeRegress.pl --list=csmith.list --sub --parallel=$testParallel");
  }
}

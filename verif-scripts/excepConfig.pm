# ------------------------------------------------------------------------------------------------- 
# Copyright (c) 2018, IIT Madras All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
# 
# - Redistributions of source code must retain the below copyright notice, this list of conditions
#   and the following disclaimer.  
# - Redistributions in binary form must reproduce the above copyright notice, this list of 
#   conditions and the following disclaimer in the documentation and/or other materials provided 
#   with the distribution.  
# - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
#   promote products derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# -------------------------------------------------------------------------------------------------
# Author: Lavanya J
# Email id: lavanya.jagan@gmail.com
# -------------------------------------------------------------------------------------------------
#
#!/usr/bin/perl

package excepConfig;

use strict;
use Exporter qw(import);
our @EXPORT = qw( %excepCount setCountValue getCountValue setCountAll clearCountAll
                  printExcepCount
                );
# ----------------------------------------------------
# Test run configurations
# <script_name>_debug : [1 -> prints debug info, 0 -> no debug
# traceConfig         : generates trace files for debug, rtl, model trace, disassembly
# fsdbConfig          : generates fsdb file
# simulatorConfig     : config for which design simulator to use
#                       [0 -> bluespec, 1 -> ncverilog, 2 -> vcs]
# ----------------------------------------------------
our %excepCount = (
                  "ecause00"    => 0,
                  "ecause01"    => 0,
                  "ecause02"    => 0,
                  "ecause03"    => 0,
                  "ecause04"    => 0,
                  "ecause05"    => 0,
                  "ecause06"    => 0,
                  "ecause07"    => 0,
                  "ecause08"    => 0,
                  "ecause09"    => 0,
                  "ecause11"    => 0,
                  "ecause12"    => 0,
                  "ecause13"    => 0,
                  "ecause15"    => 0
               );

# ------------------------------------------------
# method to set config with the arg value
# usage eg: setCountValue("ecause15", 10);
# -----------------------------------------------
sub setCountValue {
  my @args = @_;
  $excepCount{$args[0]} = $args[1];
}

# ------------------------------------------------
# method to get args value
# usage eg: getCountValue("ecause09");
# -----------------------------------------------
sub getCountValue {
  my @args = @_;
  return $excepCount{$args[0]};
}

# ------------------------------------------------
# method to get args value
# usage eg: setCountAll(10);
# -----------------------------------------------
sub setCountAll {
  my @args = @_;
  foreach my $key (keys %excepCount) {
    $excepCount{$key} = $args[0];
  }
}

# ------------------------------------------------
# method to get clear args value
# -----------------------------------------------
sub clearCountAll {
  foreach my $key (keys %excepCount) {
    $excepCount{$key} = 0;
  }
}

# ------------------------------------------------
# method to print args values
# usage eg: printConfig();
# -----------------------------------------------
sub printExcepCount {
  print "---------CONFIG------------------------------\n";
  foreach my $key (sort keys %excepCount) {
    print "$key\t\t: $excepCount{$key}\n";
  }
  print "---------------------------------------------\n";
}

1;

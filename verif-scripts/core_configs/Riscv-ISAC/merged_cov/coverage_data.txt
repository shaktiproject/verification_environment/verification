lb- 52/95
ld 66/87
lh 47/85
lw 57/83

sb - 69/222
sh-  65/214
sw- 60/212
sd- 87/214

Cross coverage data:
   '[ lb,beq : lb,beq  :: a=rd :? :: ?:rs1==a or rs2==a]': 92988 *
   '[ lbu,beq : lbu,beq  :: a=rd :? :: ?:rs1==a or rs2==a]': 3204
     '[ ld,beq : ld,beq  :: a=rd :? :: ?:rs1==a or rs2==a]': 66957 *
   '[ lh,beq : lh,beq  :: a=rd :? :: ?:rs1==a or rs2==a]': 36452 *
   '[ lhu,beq : lhu,beq  :: a=rd :? :: ?:rs1==a or rs2==a]': 3364
    '[ lw,beq : lw,beq  :: a=rd :? :: ?:rs1==a or rs2==a]': 45397 *
  '[ lwu,beq : lwu,beq  :: a=rd :? :: ?:rs1==a or rs2==a]': 17444
 241.794K branch not taken after loads

    '[ lb,bne : lb,bne  :: a=rd :? :: ?:rs1==a or rs2==a]': 74543
   '[ lbu,bne : lbu,bne  :: a=rd :? :: ?:rs1==a or rs2==a]': 57930 *
    '[ ld,bne : ld,bne  :: a=rd :? :: ?:rs1==a or rs2==a]': 142106 *
    '[ lh,bne : lh,bne  :: a=rd :? :: ?:rs1==a or rs2==a]': 392
    '[ lhu,bne : lhu,bne  :: a=rd :? :: ?:rs1==a or rs2==a]': 85406 *
   '[ lw,bne : lw,bne  :: a=rd :? :: ?:rs1==a or rs2==a]': 1527
    '[ lwu,bne : lwu,bne  :: a=rd :? :: ?:rs1==a or rs2==a]': 63306 *

348.75K Branch taken after loads

Branch after stores with one instruction in b/w ~36.42K
   '[ sw,jalr :?: sw,jalr  :: a=rs1:c=rd:? :: ?: rs1==a: rs1==c]': 36415

 store after load for different access sizes
    '[ lb,sb : lb,sb  :: a=rd:? :: ?:rs2==a]': 122754 *
    '[ lb,sb : lb,sb  :: b=rs1:? :: ?:rs1==b]': 1842987
122.75K
    '[ ld,sd : ld,sd  :: a=rd:? :: ?:rs2==a]': 254289 *
    '[ ld,sd : ld,sd  :: b=rs1:? :: ?:rs1==b]': 1572929
254.29K

    '[ lw,sw : lw,sw  :: a=rd:? :: ?:rs2==a]': 198324 *
    '[ lw,sw : lw,sw  :: b=rs1:? :: ?:rs1==b]': 258025
198.32K

 

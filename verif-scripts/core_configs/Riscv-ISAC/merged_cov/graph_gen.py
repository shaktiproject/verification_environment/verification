import numpy as np
import matplotlib.pyplot as plt

#First, Coverage for all L/S instructions
#load_cov_arr= [ 52/95, 66/87, 47/85, 57/83] * 100
#load_arr =  ['lb', 'ld', 'lh', 'lw']

load_data = {  'lb':100*52/95, 'ld':100*66/87, 'lh':100*47/85, 'lw':100*57/83}
load_instrs = list(load_data.keys())
load_cov_arr = list(load_data.values())


fig = plt.figure(figsize = (10, 5))
 
# creating the bar plot for load
plt.bar(load_instrs, load_cov_arr, color ='blue', width = 0.4)
 
plt.xlabel("Instruction ->")
plt.ylabel("Coverage in % ->")
plt.title("Overall coverage for different load instructions")
plt.show()


#store_cov_arr= [ 69/222, 65/214, 60/212, 87/214] * 100
#store_arr =  ['sb', 'sh', 'sw', 'sd']

store_data = {  'sb':100*69/222, 'sh':100*65/214, 'sw':100*60/212, 'sd':100*87/214}
store_instrs = list(store_data.keys())
store_cov_arr = list(store_data.values())


fig2 = plt.figure(figsize = (10, 5))
 
# creating the bar plot for store
plt.bar(store_instrs, store_cov_arr, color ='red', width = 0.4)
 
plt.xlabel("Instruction ->")
plt.ylabel("Coverage in % ->")
plt.title("Overall coverage for different store instructions")
plt.show()


#Cross coverage data for branches after L/S

cross_cov1_data = {  'Branch not taken after loads': 241.794, 'Branch taken after loads':348.75, 'Branch after stores':36.42}
cross_cov1_x = list(cross_cov1_data.keys())
cross_cov1_y = list(cross_cov1_data.values())

fig3 = plt.figure(figsize = (10, 8))
 
# creating the bar plot for cross_cov1
plt.bar(cross_cov1_x, cross_cov1_y, color ='green', width = 0.4)
 
plt.xlabel("Type of dependency->")
plt.ylabel("No. of occurrences (in 1000s) ->")
plt.title("Occurences of different cross-instruction dependencies")
plt.show()

#Cross coverage with stores after loads:


cross_cov2_data = {  'Byte access': 122.75, 'Double word access':254.29, 'Word access':198.32}
cross_cov2_x = list(cross_cov2_data.keys())
cross_cov2_y = list(cross_cov2_data.values())

fig4 = plt.figure(figsize = (10, 8))
 
# creating the bar plot for cross_cov2
plt.bar(cross_cov2_x, cross_cov2_y, color ='cyan', width = 0.4)
 
plt.xlabel("Access size->")
plt.ylabel("No. of occurrences (in 1000s) ->")
plt.title("Occurences of different store after load dependencies")
plt.show()




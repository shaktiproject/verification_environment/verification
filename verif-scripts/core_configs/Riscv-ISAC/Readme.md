
**Commands to run to generate CGF for one test**:\
spike --log-commits --log dump --isa=rv64 +signature=signature +signature-granularity=4 ${TNAME}.elf

riscv_isac --verbose debug coverage -t dump -c dataset.cgf -c rv64i_LS.cgf --parser-name spike -o output.cgf -x 64 -e ${TNAME}.elf\
**Cross_cov label examples**:\
    cross_comb:
      '[ add,sub : add,sub  :: a=rd :? :: ?:rs1==a or rs2==a]': 0

**Command to run to merge all output cgfs**:\
riscv_isac --verbose debug merge -c output_test1.cgf output_test2.cgf ... for all cgfs -o final_merge.cgf -x 64

Note-remove the "total_coverage" label from each tests output cgf otherwise merge wont work.

